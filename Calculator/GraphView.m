//
//  GraphView.m
//  Calculator
//
//  Created by Daniel Hirschlein on 12/19/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "GraphView.h"
#import "AxesDrawer.h"


@implementation GraphView

#define DEFAULT_SCALE 1.5

@synthesize dataSource = _dataSource;
@synthesize scale = _scale;
@synthesize origin = _origin;
@synthesize dotModeSelected = _dotModeSelected;

- (CGFloat)scale
{
    if (!_scale)
    {
        return DEFAULT_SCALE;
    }
    else
        return _scale;
}

- (void)setScale:(CGFloat)scale
{
    if(scale != _scale){
        _scale = scale;
        [self setNeedsDisplay];
    }
}

- (void)setOrigin:(CGPoint)origin   
{
    if(origin.x != _origin.x && origin.y != _origin.y)
    {
        _origin = origin;
        [self setNeedsDisplay];
    }
}

- (void)pinch:(UIPinchGestureRecognizer *)gesture
{
    if((gesture.state == UIGestureRecognizerStateChanged) ||
       (gesture.state == UIGestureRecognizerStateEnded))
    {
        self.scale *= gesture.scale;
        gesture.scale = 1;
    }
}

- (void)setup
{
    self.contentMode = UIViewContentModeRedraw;
}

- (void)awakeFromNib
{
    [self setup];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}


- (void)drawRect:(CGRect)rect
{   
    // draw axes
    [AxesDrawer drawAxesInRect:self.bounds originAtPoint:self.origin scale:self.scale];
    
    // draw plots
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(context, 5.0);
    [[UIColor blueColor] setStroke];

    NSArray* plots = [self.dataSource plotsForGraphView:self];
    
    if(plots.count > 0)
    {
        CGPoint startPoint = [[plots objectAtIndex:0] CGPointValue];
        CGContextMoveToPoint(context, startPoint.x, startPoint.y);
            
        for(NSValue *val in plots)
        {
            CGPoint point = [val CGPointValue];
            
            BOOL boundsContainsPoint = CGRectContainsPoint(self.bounds, point);
            if (boundsContainsPoint) {
                
                if(self.dotModeSelected)
                    CGContextFillRect(context, CGRectMake(point.x,point.y,1,1));
                else
                {
                    CGContextAddLineToPoint(context, point.x, point.y );
                    CGContextStrokePath(context);
                }
            }
            CGContextMoveToPoint(context, point.x , point.y );
        }
    }
}



@end
