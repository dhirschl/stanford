//
//  HappinessViewController.h
//  Happiness
//
//  Created by Daniel Hirschlein on 12/15/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HappinessViewController : UIViewController

@property (nonatomic) int happiness; // 0 is sad; 100 is very happy

@end
